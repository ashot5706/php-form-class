# PHP form class
class for quick creating form with input elements

# Usage:

*//Create variable with this class*  
`$variableName = new form;`

*//Change method attribute, by default GET*   
`$variableName->method = "GET";` *//or "POST"*

*//Change action attribute, by default index.php*  
`$variableName->action = "filename.extension";` *//action filename*

*//Create new input element*  
`$variableName->input(["attributename"=>"value", "attributename"=>"value", ...]);`

*//Create new input element with type password*  
`$variableName->password(["attributename"=>"value", "attributename"=>"value, ..."]);`

*//Create new input element with type submit*  
`$variableName->submit(["attributename"=>"value", "attributename"=>"value", ...]);`

*//Create textarea element*  
`$variableName->textarea("your text", [columns{by default 50}], [rows{by default 4}]);`

*//Apply changes and echo form*  
`$variableName->createForm();`  
<strong>//Remember after using createForm() You can't create any inputs with same variable, that will be return error</strong>

