<?php
    class form{
        public $method = "GET"; //form method, default GET
        public $action = "index.php"; //form action, default index.php
        private $createflag = false; //flag, show that form already created
        private $input = ""; // inputs list
        private $submit = "";// input type=submit list
        private $password = ""; // input type=password list
        private $textarea = ""; // textarea tags list


        //create input elemnt
        public function input($attributes){
            //error handling
            try{
                if($this->createflag){
                    throw new Exception("method input must be before method createForm");
                }
            }catch(Exception $e) {
                echo 'Error Message: ' .$e->getMessage();
            }

            //break function on error
            if($this->createflag) return;

            //add to input elements list
            $this->input .= "<input ";
            //set attributes
            foreach ($attributes as $name=>$value){
                $this->input .= $name . "=\"" . $value . "\" ";
            }
            $this->input .= "/><br>";
        }

        //create input element with "submit" type
        public function submit($attributes){
            //error handling
            try{
                if($this->createflag){
                    throw new Exception("method submit must be before method createForm");
                }
            }catch(Exception $e) {
                echo 'Error Message: ' .$e->getMessage();
            }

            //break function on error
            if($this->createflag) return;

            //add to 'input type=submit' elements list
            $this->submit .= "<input type=\"submit\" ";
            //set attributes
            foreach ($attributes as $name=>$value){
                $this->submit .= $name . "=\"" . $value . "\" ";
            }
            $this->submit .= "/><br>";
        }

        //create input element with "password" type
        public function password($attributes){
            //error handling
            try{
                if($this->createflag){
                    throw new Exception("method password must be before method createForm");
                }
            }catch(Exception $e) {
                echo 'Error Message: ' .$e->getMessage();
            }

            //break function on error
            if($this->createflag) return;

            //add to 'input type=password' elements list
            $this->password .= "<input type=\"password\"";
            //set attributes
            foreach ($attributes as $name=>$value){
                $this->password .= $name . "=\"" . $value . "\" ";
            }
            $this->password .= "/><br>";
        }

        //create textarea element
        //usage $object->textarea(text,[cols{50 default}],[rows{4 default}])
        public function textarea($text, $cols = 50, $rows = 4){
            //error handling
            try{
                if($this->createflag){
                    throw new Exception("method textarea must be before method createForm");
                }
            }catch(Exception $e) {
                echo 'Error Message: ' .$e->getMessage();
            }

            //break function on error
            if($this->createflag) return;

            //add to textarea elements list;
            $this->textarea .= "<textarea "."cols=\"".$cols."\" rows=\"".$rows."\">";
            $this->textarea .= $text;
            $this->textarea .= "</textarea><br>";
        }

        //create and echo form wich contains all before maded inputs
        public function createForm(){

            //string for echo
            $html =  "<form" .
                " action=\"" . $this->action . "\" method=\"" . $this->method . "\">" .
                $this->input .
                $this->password .
                $this->textarea .
                $this->submit .
                "</form>";
            echo $html;
            $this->createflag = true;
        }
    }

    //examples
    
    //true
    $input = new form;
    $input->action="index.php";
    $input->method="GET";
    $input->input(["type" => "text", "value"=>"input field"]);
    $input->password(["placeholder"=>"password"]);
    $input->textarea("textsnjfv ghjvsb adhcvsdhj gvhjsd xvhj xdvb hndb hdbvhd s");
    $input->submit(["value"=>"OK"]);
    $input->createForm();
    
    //false
    $input = new form;
    $input->action="index.php";
    $input->method="GET";
    $input->password(["placeholder"=>"password"]);
    $input->textarea("textsnjfv ghjvsb adhcvsdhj gvhjsd xvhj xdvb hndb hdbvhd s");
    $input->submit(["value"=>"OK"]);
    $input->createForm();
    $input->input(["type" => "text", "value"=>"input field"]); //this will be return error// line 17
    $input->createForm();
?>